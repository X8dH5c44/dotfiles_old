import XMonad
import Data.Monoid
import System.Exit

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import Data.List
import XMonad.Layout.Spacing
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Util.Run
import Graphics.X11.ExtraTypes.XF86

myTerminal           = "gnome-terminal"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse  = True
myClickJustFocuses  :: Bool
myClickJustFocuses   = False
myBorderWidth        = 1
myModMask            = mod4Mask
myWorkspaces         = ["N°1","N°2","N°3","N°4","N°5","N°6","N°7","N°8","N°9"]
myNormalBorderColor  = "#000000"
myFocusedBorderColor = "#aaaaaa"
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    -- custom keys
    [ ((0, xF86XK_AudioRaiseVolume      ), spawn "amixer set Master 2+")
    , ((0, xF86XK_AudioLowerVolume      ), spawn "amixer set Master 2-")
    , ((0, xF86XK_AudioMute             ), spawn "pactl set-sink-mute 0 toggle")
    , ((0, xF86XK_AudioPlay             ), spawn "playerctl play-pause")
    , ((0, xF86XK_AudioPrev             ), spawn "playerctl previous")
    , ((0, xF86XK_AudioNext             ), spawn "playerctl next")
    , ((0, xF86XK_MonBrightnessUp       ), spawn "lux -a 5%")
    , ((0, xF86XK_MonBrightnessDown     ), spawn "lux -s 5%")
   -- , ((0, xK_Super_L                   ), spawn "dmenu_run")
   -- , ((0, xK_Super_R                   ), spawn "dmenu_run")
    , ((modm,               xK_p     ), spawn "dmenu_run")
    -- standard keys
    , ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm .|. shiftMask, xK_c     ), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modm,               xK_n     ), refresh) -- Resize viewed windows to the correct size ???
    , ((modm,               xK_Tab   ), windows W.focusDown)
    , ((modm,               xK_j     ), windows W.focusDown)
    , ((modm,               xK_k     ), windows W.focusUp  )
    , ((modm,               xK_m     ), windows W.focusMaster) -- Move focus to the master window
    , ((modm,               xK_Return), windows W.swapMaster)
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
    , ((modm,               xK_h     ), sendMessage Shrink)
    , ((modm,               xK_l     ), sendMessage Expand)
    , ((modm,               xK_t     ), withFocused $ windows . W.sink) -- Push window back into tiling
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1)) -- Increment the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1))) -- Deincrement the number of windows in the master area
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9] -- switch workspaces
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]] -- move to workspace
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..] -- switch to monitor 1, 2, 3
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]] -- move to monitor 1, 2, 3
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w))]
    -- you may also bind events to the mouse scroll wheel (button4 and button5)
myLayout = avoidStruts (tiled) ||| Full
  where
    tiled         = gaparound $ windowspacing $ Tall nmaster delta ratio
    windowspacing = spacing 10
    gaparound     = gaps [(U,10),(D,10),(L,10),(R,10)]
    nmaster       = 1
    ratio         = 2/3
    delta         = 3/100
myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]
myEventHook = mempty
myStartupHook = setWMName "LG3D"

main = do
  xmproc0 <- spawnPipe "killall xmobar; xmobar -x 0 ~/.config/xmobarrc"
  xmproc1 <- spawnPipe "source /home/kolja/.xsessionrc"
  xmproc2 <- spawnPipe "killall xcompmgr; xcompmgr -o 1.0"
  xmonad $ docks $ def
        { terminal           = myTerminal
        , focusFollowsMouse  = myFocusFollowsMouse
        , clickJustFocuses   = myClickJustFocuses
        , borderWidth        = myBorderWidth
        , modMask            = myModMask
        , workspaces         = myWorkspaces
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , keys               = myKeys
        , mouseBindings      = myMouseBindings
        , layoutHook         = smartBorders $ myLayout
        , manageHook         = myManageHook
        , handleEventHook    = myEventHook
        , logHook            = dynamicLogWithPP $ myPPConfig { ppOutput = hPutStrLn xmproc0 }
        , startupHook        = myStartupHook }

monokaiRed    = "#f92772"
monokaiPurple = "#ae81ff"
monokaiGreen  = "#a7e22e"
monokaiBlue   = "#66d9ee"
monokaiOrange = "#fe9720"
monokaiYellow = "#e6da74"
monokaiGrey   = "#808080"

modKey        = "super"

myPPConfig = xmobarPP { ppOrder           = \(ws:_:t:_) -> [ws,t]
                      , ppTitle           = xmobarColor monokaiPurple "" . shorten 70
                      , ppWsSep           = ""
                      , ppCurrent         = xmobarColor monokaiRed "" . clickWorkspace "[" "]"
                      , ppVisible         = xmobarColor monokaiOrange "" . clickWorkspace "[" "]"
                      , ppHidden          = xmobarColor monokaiYellow "" . clickWorkspace " " " "
                      , ppHiddenNoWindows = xmobarColor "grey" "" . clickWorkspace " " " " }
             where
                clickWorkspace a b ws = "<action=xdotool key " ++ modKey ++ "+" ++ show(index) ++ ">" ++ a ++ ws ++ b ++ "</action>" where
		    wsIdxToString Nothing = "1"
		    wsIdxToString (Just n) = show $ mod (n+1) $ (length myWorkspaces) + 1 -- if you use 10 ws and 10 is bind to 0 remove the +1
		    index = wsIdxToString (elemIndex ws myWorkspaces)
